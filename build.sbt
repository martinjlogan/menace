import Dependencies._

ThisBuild / scalaVersion     := "2.13.2"
ThisBuild / version          := "1.0"
ThisBuild / organization     := "com.martinjlogan"
ThisBuild / organizationName := "martinjlogan"

lazy val root = (project in file("."))
  .settings(
    name := "tictactoe",
    libraryDependencies += scalaTest % Test
  )

libraryDependencies += "org.typelevel" %% "cats-core" % "2.0.0"

// scalac options come from the sbt-tpolecat plugin so need to set any here

addCompilerPlugin("org.typelevel" %% "kind-projector" % "0.11.0" cross CrossVersion.full)

// See https://www.scala-sbt.org/1.x/docs/Using-Sonatype.html for instructions on how to publish to Sonatype.
