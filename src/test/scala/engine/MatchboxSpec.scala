package engine

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class MatchboxSpec extends AnyFlatSpec with Matchers {
  "The Box" should "initialize" in {
    val eb = Gameboard.emptyBoard

    Box.init(Seq[Int](), Seq[Int]()) shouldEqual Box(Gameboard.emptyBoard, Box.drawer)
    Box.init(Seq(5), Seq(1)) shouldEqual Box(Gameboard.mark(eb, Seq(5), Seq(1)),
                                             Vector(1, 0, 1, 1, 1, 0, 1, 1, 1))
    Box.init(Seq(5,3,2), Seq(1,0)) shouldEqual Box(Gameboard.mark(eb, Seq(2,3,5), Seq(0,1)),
                                                   Vector(0, 0, 0, 0, 1, 0, 1, 1, 1))

    Box(Gameboard.emptyBoard) shouldEqual Box(Gameboard.emptyBoard, Box.drawer)
  }

  "The Box" should "expand its drawer" in {
    Box.expandDrawer(Vector(0,0,1,3)) shouldEqual Vector(2,3,3,3)
    Box.expandDrawer(Vector(0,0,1,3,1,0,0,4,0)) shouldEqual Vector(2,3,3,3,4,7,7,7,7)
  }

  "The Box" should "update its drawer" in {
    Box.updateDrawer(Box(Vector(E), Vector(0,0,1,3)), 0, 2) shouldEqual Box(Vector(E), Vector(2,0,1,3))
  }
}
