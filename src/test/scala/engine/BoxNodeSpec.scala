package engine

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class BoxNodeSpec extends AnyFlatSpec with Matchers {
  "BoxNode" should "generate all possible game states" in {
    val gameStates = BoxNode.gameStatesToUniqueStatesList(BoxNode.generateGameStates)
    println(gameStates.length)
  }

  "BoxNode" should "create translated unique node tree" in {
  }
}
