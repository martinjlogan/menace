package engine

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class GameboardSpec extends AnyFlatSpec with Matchers {
  "The Gameboard" should "be able to be marked and find remaining moves left" in {
    val b  = Gameboard.emptyBoard
    val b2 = Gameboard.mark(b, Seq(5), X)
    val b3 = Gameboard.mark(b2 ,Seq(2), O)
    val b4 = Gameboard.mark(Gameboard.mark(b3, Seq(1,3), X), Seq(0), O)
    val b5 = Gameboard.mark(Gameboard.mark(b4, Seq(7), X), Seq(4), O)
    val b6 = Gameboard.mark(Gameboard.mark(b5, Seq(8), X), Seq(6), O)
    val b7 = Gameboard.mark(Gameboard.mark(b5, Seq(6), X), Seq(8), O)

    Gameboard.findOpenMoves(b2) shouldEqual (O,List(8, 7, 6, 4, 3, 2, 1, 0))
    Gameboard.findOpenMoves(b3) shouldEqual (X,List(8, 7, 6, 4, 3, 1, 0))
    Gameboard.findOpenMoves(b4) shouldEqual (O,List(8, 7, 6, 4))
    Gameboard.findOpenMoves(b5) shouldEqual (O,List(8, 6))
    Gameboard.findOpenMoves(b6) shouldEqual (O,List[Int]())
    Gameboard.findOpenMoves(b7) shouldEqual (O,List[Int]())
    Gameboard.print(b6)
    Gameboard.print(b7)
  }

  "checkForWinner" should "tell us who won or if moves are left to take" in {
    val b  = Gameboard.emptyBoard
    Gameboard.checkForWinner(Gameboard.mark(b, Seq(2,4,6), X)) shouldEqual X
    Gameboard.checkForWinner(Gameboard.mark(b, Seq(1,4,7), X)) shouldEqual X
    Gameboard.checkForWinner(Gameboard.mark(b, Seq(1,4), X))   shouldEqual E
  }

  /**
    * Each new set is a quarter turn clockwise from the previous
    * 0 1 2    6 3 0   8 7 6   2 5 8
    * 3 4 5 -> 7 4 1 + 5 4 3 + 1 4 7
    * 6 7 8    8 5 2   2 1 0   0 3 6
    */
  "createRotatedSet" should "quarter turn a board 3 times" in {
    val eb = Gameboard.emptyBoard
    val v  = Gameboard.mark(eb, Seq(0,2), Seq(1))
    val v2 = Gameboard.mark(eb, Seq(2,8), Seq(5))
    val v3 = Gameboard.mark(eb, Seq(6,8), Seq(7))
    val v4 = Gameboard.mark(eb, Seq(0,6), Seq(3))
    val a :: a2 :: a3 :: a4 :: _ = Gameboard.createRotatedSet(v)
    a3 shouldEqual Vector(E, E, E, E, E, E, X, O, X)
  }
  /**
    *          horiz   vert   diag-l /  diag-r \
    * 0 1 2    6 7 8   2 1 0   8 5 2   0 3 6
    * 3 4 5 -> 3 4 5 + 5 4 3 + 7 4 1 + 1 4 7
    * 6 7 8    1 2 3   8 7 6   6 3 0   2 5 8
    */
  "createMirroredSet" should "create 4 reflections about all axis" in {
    val eb = Gameboard.emptyBoard
    val v  = Gameboard.mark(eb, Seq(0,2), Seq(1))
    val v2 = Gameboard.mark(eb, Seq(6,8), Seq(7))
    val v3 = Gameboard.mark(eb, Seq(0,2), Seq(1))
    val v4 = Gameboard.mark(eb, Seq(2,8), Seq(5))
    val v5 = Gameboard.mark(eb, Seq(0,6), Seq(3))
    val a :: a2 :: a3 :: a4 :: a5 :: _ = Gameboard.createMirroredSet(v)
  }
}
