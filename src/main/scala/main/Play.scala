package main

// Question: what is the canonical way to organize packages in a project
import engine._

object Play {
  // Question: do people put functions above their callers or below in Scala

  /** this function makes a move on behalf of a user - human or computer
    *
    * returns the Mark of the winner and the list of played boards
    * TODO this is rather unsafe
    * @playHistory a list of the boards making up the the current game
    * @gameStates a list of all possible states of play. To be updated after game.
    */
  def makeMoves(currentBoard:Gameboard.Board, playHistory:List[(Int, Int)], gameStates:Vector[Box]):(Mark, List[(Int,Int)]) = {
    val (mark, possibleMoves) = Gameboard.findOpenMoves(currentBoard)
    mark match {
      case _ if(possibleMoves.length == 0) =>
        // we have a winner or a tie
        println("")
        Gameboard.print(currentBoard)
        (mark, playHistory)
      case X  =>
        val (move, gameStateCode) = Box.pickMove(currentBoard, gameStates).getOrElse((10, (-1, -1)))
        val nextBoard = Gameboard.mark(currentBoard, List(move), mark)
        if(!possibleMoves.contains(move)) {
          Gameboard.print(currentBoard)
          throw new Exception(s"move $move not in $possibleMoves")
        }
        makeMoves(nextBoard, gameStateCode :: playHistory, gameStates)
      case O =>
        Gameboard.print(currentBoard)
        val move = scala.io.StdIn.readLine(s"Pick a Move for $mark > ").toInt
        possibleMoves.contains(move) match {
          case true =>
            val nextBoard = Gameboard.mark(currentBoard, List(move), mark)
            val (b, gameStateCode) = Box.findBoxForPosition(currentBoard, gameStates)
            makeMoves(nextBoard, playHistory, gameStates) // put O moves in too to speed training
          case false =>
            println(s"$move is not an available move. Choose again")
            makeMoves(currentBoard, playHistory, gameStates)
        }
    }
  }

  def main(args: Array[String]): Unit = {
    println("Welcome to tic tac toe!")
    val gameStates = Learn.loadGameStates
    val (mark, playHistory) = makeMoves(Gameboard.emptyBoard, List[(Int, Int)](), gameStates)
    val smarterGameStates = Learn.train(mark, playHistory, gameStates)
    Learn.persistGameStates(smarterGameStates)
    mark match {
      case E => println("The match was a tie.")
      case _ => println(s"$mark won the match!")
    }
  }
}
