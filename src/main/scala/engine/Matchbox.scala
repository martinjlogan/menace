package engine

/** contains the basic structures of our matchbook
  *
  */

/** This case class emulates our matchbook.
  *
  * It has a vector for newPositions and a drawer containing
  * the number of moves available for each position
  *
  * @b a vector 0 to 8 with marks for X O or E empty.
  * @drawer a vector containing the number of "beads" for each position
  */
case class Box(board:Gameboard.Board, drawer:Vector[Int])

object Box {
  val drawer = Vector(1,1,1,1,1,1,1,1,1) // 1 available move for each of 9 possible moves

  /** initialize a matchbox with game board. Ensure drawer cooresponds. */
  def apply(b:Gameboard.Board):Box = {
    def buildDrawer(bl:List[Mark], d:Vector[Int], i:Int):Vector[Int] = bl match {
      case E :: t => buildDrawer(t, 1 +: d, i+1)
      case h :: t    => buildDrawer(t, 0 +: d, i+1)
      case Nil       => d.reverse
    }
    Box(b, buildDrawer(b.toList, Vector[Int](), 0))
  }

  /** initialize a matchbox with Xs and Os
    *
    * @xs the b to place Xs
    * @os the b to place Os
    * Question: how do I constrain the number of elements that can be in board
    */
  def init(xs:Seq[Int], os:Seq[Int]):Box = {
    Box(Gameboard.mark(Gameboard.mark(Gameboard.emptyBoard, xs, X), os, O))
  }

  /** Find a board amongst all game states and then use its drawer to find a move */
  def pickMove(b:Gameboard.Board, gameStates:Vector[Box]):Option[(Int, (Int, Int))] = {
    val (box, gameStateIndex) = findBoxForPosition(b, gameStates)
    val moves = expandDrawer(box.drawer)
    val transVector =
      if(b != box.board) Gameboard.translations(Gameboard.identifyTranslation(b, box.board))
      else Vector(0,1,2,3,4,5,6,7,8)
    //tranlate from one box to another /
    val r = new scala.util.Random(System.currentTimeMillis)
    val move = moves(r.nextInt(moves.length))
    Some((transVector(move), (gameStateIndex, move)))
  }

  // TODO how to update this intelligently. Can find a box but need to update beads
  def findBoxForPosition(b:Gameboard.Board, allGameStates:Vector[Box]):(Box, Int) = {
    def findBoxForPosition(b:Gameboard.Board, boxList:List[Box], i:Int, translatedBoards:List[Gameboard.Board]):(Box, Int) = boxList match {
      case h :: t if(translatedBoards.contains(h.board)) =>
        (h -> i)
      case _ :: t =>
        findBoxForPosition(b, t, i+1, translatedBoards)
      case Nil =>
        throw new Exception(s"boxVector should contain all possible game states $b")
    }

    findBoxForPosition(b, allGameStates.toList, 0, Gameboard.createTranslatedSet(b))
  }


  def expandDrawer(d:Vector[Int]):Vector[Int] = {
    def expandDrawer(d:List[Int], i:Int, acc:Vector[Int]):Vector[Int] = d match {
      case Nil    => acc
      case 0 :: t => expandDrawer(t, i+1, acc)
      case n :: t => expandDrawer(t, i+1, List.fill(n)(i).toVector ++ acc)
    }

    expandDrawer(d.toList, 0, Vector[Int]()).reverse
  }

  /** update the drawer of a box
    *
    * @b the box to update
    * @i the index of the drawer to update
    * @v the value to add to the index
    */
  def updateDrawer(b:Box, i:Int, v:Int):Box = {
    println(s"update drawer $b at $i with $v")
    val newDrawer = b.drawer.updated(i, b.drawer(i) + v)
    Box(b.board, newDrawer)
  }
}
