package engine

/** Functions that manipulate a Gameboard
  *
  * The gameboard is a structure containing spaces 0-8 representing
  * the squares on a tic tac toe board
  * This object exports the following Functions
  *  print/1, mark/3, checkForWinner/1, findOpenMoves/1, createRotatedSet/1
  *  createMirroredSet/1
  */

/** sum type for a mark on the board X O and E for empty */
sealed trait Mark
case object X extends Mark
case object O extends Mark
case object E extends Mark

sealed trait Translation
case object Rotate1 extends Translation
case object Rotate2 extends Translation
case object Rotate3 extends Translation
case object Horizontal extends Translation
case object Vertical extends Translation
case object DiagL extends Translation
case object DiagR extends Translation

object Gameboard {
  type Board = Vector[Mark]
  val emptyBoard = Vector(E, E, E, E, E, E, E, E, E)
  val translations = Map[Translation, List[Int]](
    Rotate1 -> List(6, 3, 0, 7, 4, 1, 8, 5, 2),
    Rotate2 -> List(8, 7, 6, 5, 4, 3, 2, 1, 0),
    Rotate3 -> List(2, 5, 8, 1, 4, 7, 0, 3, 6),
    Horizontal -> List(6, 7, 8, 3, 4, 5, 0, 1, 2),
    Vertical -> List(2, 1, 0, 5, 4, 3, 8, 7, 6),
    DiagL -> List(8, 5, 2, 7, 4, 1, 6, 3, 0),
    DiagR -> List(0, 3, 6, 1, 4, 7, 2, 5, 8)
  )

  /** pretty print a game board */
  def print(b:Board):Unit = {
    val f = {n:Int =>
              val p = b(n)
              p match {
                case E => System.out.print(s"$n ")
                case _ => System.out.print(s"$p ")
              }}
    (0 to 2).toList.foreach(f); println("")
    (3 to 5).toList.foreach(f); println("")
    (6 to 8).toList.foreach(f); println("\n")
  }

  /** raw print a game board */
  def printRaw(b:Board):Unit = {
    val f = {n:Int =>
              val p = b(n)
              System.out.print(s"$p ")
            }
    (0 to 2).toList.foreach(f); println("")
    (3 to 5).toList.foreach(f); println("")
    (6 to 8).toList.foreach(f); println("\n")
  }

  /** Mark the board
    *
    * @b is the game board
    * @xos is a sequence of the moves that are to be marked
    * @mk is the mark to be put down for the moves taken
    */
  def mark(b:Board, xos:Seq[Int], mk:Mark):Board = xos match {
    case h :: t => mark(b.updated(h, mk), t, mk)
    case Nil    => b
  }

  /** Mark the board
    *
    * @b is the game board
    * @xs is a sequence of the X moves that are to be marked
    * @os is a sequence of the O moves that are to be marked
    */
  def mark(b:Board, xs:Seq[Int], os:Seq[Int]):Board = mark(mark(b, xs, X), os, O)

  /** Check for a winner on a gameboard.
    *
    * Return the winner or E for remaining empty space
    * Note will not function if the game board is full.
    */
  def checkForWinner(b:Board):Mark = {
    def checkWinners(b:Board, winners:List[List[Int]]):Mark = winners match {
      case h :: t =>
        checkWinner(b, h, b(h.head)) match {
          case E => checkWinners(b, t)
          case m => m
        }
      case Nil => E
    }

    def checkWinner(b:Board, winner:List[Int], mark:Mark):Mark = winner match {
      case h :: t =>
        b(h) match {
          case E      => E
          case `mark` => checkWinner(b, t, mark)
          case _      => E
        }
      case Nil =>
          mark
    }

    val winners = List(
                List(0,1,2), List(3,4,5), List(6,7,8),
                List(0,3,6), List(1,4,7), List(2,5,8),
                List(0,4,8), List(2,4,6))

    checkWinners(b, winners)
  }


  /** For a gameboard find the list of empty moves and which player can take them
    *
    * If the board is full the sequence of available moves will be empty
    * E with no open moves indicates a draw
    */
  def findOpenMoves(b:Board):(Mark, Seq[Int]) = {
    def findOpenMoves(b:List[Mark], index:Int, countO:Int,
                      countX:Int, possMoves:List[Int]):(Mark, Seq[Int]) = b match {
      case X :: t    => findOpenMoves(t, index+1, countO, countX+1, possMoves)
      case O :: t    => findOpenMoves(t, index+1, countO+1, countX, possMoves)
      case E :: t => findOpenMoves(t, index+1, countO, countX, index :: possMoves)
      case Nil       => (if(countX == countO) X else O, possMoves)
    }

    checkForWinner(b) match {
      case E =>
        findOpenMoves(b.toList, 0, 0, 0, List[Int]()) match {
          case (_, l) if(l.isEmpty) => (E, List[Int]()) // a tie
          case x               => x
        }
      case m => (m, List[Int]())
    }
  }

  def createTranslatedSet(b:Board):List[Board] = {
    createRotatedSet(b) ++ createMirroredSet(b).tail
  }

  /** Create a set of the three boards that are alike but rotated
    *
    * Each new set is a quarter turn clockwise from the previous
    * 0 1 2    6 3 0   8 7 6   2 5 8
    * 3 4 5 -> 7 4 1 + 5 4 3 + 1 4 7
    * 6 7 8    8 5 2   2 1 0   0 3 6
    *
    * TODO: Abelian groups and rotation are somehow related - look at the catz class
    */
  def createRotatedSet(b:Board):List[Board] = {
    val b1 = translate(b, translations(Rotate1))
    val b2 = translate(b, translations(Rotate2))
    val b3 = translate(b, translations(Rotate3))
    List(b,b1,b2,b3)
  }

  /** create mirrored sets for a given boards
    *
    * Each new set is a reflection of the original
    *          horiz   vert   diag-l /  diag-r \
    * 0 1 2    6 7 8   2 1 0   8 5 2   0 3 6
    * 3 4 5 -> 3 4 5 + 5 4 3 + 7 4 1 + 1 4 7
    * 6 7 8    1 2 3   8 7 6   6 3 0   2 5 8
    */
  def createMirroredSet(b:Board):List[Board] = {
    val b1 = translate(b, translations(Horizontal))
    val b2 = translate(b, translations(Vertical))
    val b3 = translate(b, translations(DiagL))
    val b4 = translate(b, translations(DiagR))
    List(b,b1,b2,b3,b4)
  }

  /** translate a board by a translation vector */
  def translate(b:Board, tr:List[Int]):Board = {
    tr.foldLeft(Vector[Mark]()){(acc, n) => b(n) +: acc}.reverse
  }

  /** Identify which translation changes b1 into b2 */
  def identifyTranslation(b:Board, b2:Board):Translation = {
    def iT(b:Board, b2:Board, keys:List[Translation]):Translation = keys match {
      case key :: _ if(translate(b, translations(key)) == b2) => key
      case _ :: t => iT(b, b2, t)
      case Nil => throw new Exception("this can't happen has to be a translation")
    }
    iT(b, b2, translations.keys.toList)
  }
}
