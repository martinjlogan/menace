package engine

import java.io._
import scala.io.Source

/** This file contains functions to train the AI and to manage training data */
object Learn {

    /** given play history and the mark of who won update the matchboxes w new weights */
    def train(mark:Mark, playHistory:List[(Int, Int)], gameStates:Vector[Box]):Vector[Box] = playHistory match {
      case (index, move) :: t =>
        // stands for win lose or draw. Determines how many points to add or take away based on who won
        val box = gameStates(index)
        val newBox = Box.updateDrawer(box:Box, move, wld(mark))
        val newGameStates = gameStates.updated(index, newBox)
        train(mark, t, newGameStates)
      case Nil =>
        gameStates
    }

    def wld(mark:Mark):Int = mark match {
      case X => 2
      case O => -1
      case E => 1
    }

    /** load the last game states from storage */
    def loadGameStates:Vector[Box] = {
      try {
        readFromFile
      } catch {
        case _: Throwable =>
          println("Generating new game states")
          BoxNode.boardListToBoxVector(BoxNode.gameStatesToUniqueStatesList(BoxNode.generateGameStates))
      }
    }

    def readFromFile:Vector[Box] = {
      val bufferedSource = Source.fromFile("/tmp/gameStates.txt")
      val gameStates =
        for {
            line <- bufferedSource.getLines
            cols = line.split(" ").map(_.trim)
            board:Vector[Mark] = (0 to 8).toList.foldLeft(Vector[Mark]()){(acc, i) => acc :+ toM(cols(i))}
            drawer:Vector[Int] = (9 to 17).toList.foldLeft(Vector[Int]()){(acc, i) => acc :+ cols(i).toInt}
          } yield {
            Box(board, drawer)
          }


      val gs = gameStates.toIndexedSeq.toVector
      bufferedSource.close // Question: seems to be a timing issue here. 1 line up and it blows up
      if(gs.length == 765) {
        println("Game state loaded")
        gs
      } else
        BoxNode.boardListToBoxVector(BoxNode.gameStatesToUniqueStatesList(BoxNode.generateGameStates))
    }

    def toM(s:String):Mark = s match {
      case "X" => X
      case "O" => O
      case "E" => E
    }


      //BoxNode.boardListToBoxVector(BoxNode.gameStatesToUniqueStatesList(BoxNode.generateGameStates))

    /** load the last game states to storage */
    def persistGameStates(gameStates:Vector[Box]):Unit = {
      val file = new File("/tmp/gameStates.txt")
      val bw = new BufferedWriter(new FileWriter(file))
      for (box <- gameStates) {
          bw.write(boxToCSV(box))
      }
      bw.close()
    }

    def boxToCSV(b:Box):String = {
      val f = {(acc:String, n:Any) => acc + s"$n "}
      b.board.foldLeft("")(f) + b.drawer.foldLeft("")(f) + "\n"
    }
}
