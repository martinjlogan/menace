package engine

import scala.util.{Try, Success, Failure}

/** A container to build a representation of all possible tic tac toe GameStates
  *
  * @box is [[Box]]
  * @boxes is a map of all the boxes states beneath a particular filled position
  *
  * {{{
  *  BoxNode.generateGameStates
  * }}}
  */
case class BoxNode(box:Box, boxes:Map[Int, BoxNode]) {
  def count:Int = this.fold(0){(acc, boxNode) => acc + 1}

  /** Take the full tree of ordered game states and generate a list */
  def toList:List[Box] = this.fold(List[Box]()){(acc, boxNode) => boxNode.box :: acc}

  def fold[A](acc:A)(f:(A, BoxNode) => A):A = this.boxes match {
    case m if(m.isEmpty) =>
      f(acc, this)
    case m =>
      val newAcc = f(acc, this)
      this.boxes.foldLeft(newAcc){case (accP, (k, boxNode)) => boxNode.fold(accP)(f)}
  }
}

object BoxNode {
  /** Generate a tree of all possible boards. */
  def generateGameStates:BoxNode =
    buildTree(BoxNode(Box(Gameboard.emptyBoard), Map[Int, BoxNode]()))

  /** build the entire tree that extends from the supplied boxnode */
  def buildTree(bn:BoxNode):BoxNode = {
    val newBoards = dedupeMoves(translatedBoardsForAllMoves(bn.box.board))
    val boxes = newBoards.foldLeft(Map[Int,BoxNode]()){
      case (acc, (move, board)) =>
        val newBoxNode = BoxNode(Box(board), Map[Int, BoxNode]())
        acc.+(move -> buildTree(newBoxNode))
    }
    BoxNode(bn.box, boxes)
  }

  /** create the boards and all their rotations */
  def translatedBoardsForAllMoves(board:Gameboard.Board):Map[Int, List[Gameboard.Board]] = {
    val (xOrO, availableMoves) = Gameboard.findOpenMoves(board)
    availableMoves.foldLeft(Map[Int,List[Gameboard.Board]]()) {
      (acc, move) =>
        val newBoard = Gameboard.mark(board, List(move), xOrO)
        acc.+(move -> Gameboard.createTranslatedSet(newBoard).distinct)
    }
  }

  def dedupeMoves(b:Map[Int, List[Gameboard.Board]]):Map[Int,Gameboard.Board] = {
    def dedupeMoves(keys:List[Int], boards:Map[Int, List[Gameboard.Board]]):Map[Int,List[Gameboard.Board]] = keys match {
      case key :: t =>
          val deDuped = Try(dedupeMove(boards(key).head, boards - key))
          deDuped match {
            case Success(dd) =>
              dedupeMoves(t, dd + (key -> boards(key)))
            case Failure(_) =>
              dedupeMoves(t, boards)
          }
      case Nil =>
        boards
    }
    val ddm = dedupeMoves(b.keys.toList, b)
    ddm.foldLeft(Map[Int,Gameboard.Board]()){case (acc, (k, v)) => acc + (k -> v.head)}
  }


  def dedupeMove(board:Gameboard.Board, boards:Map[Int, List[Gameboard.Board]]):Map[Int, List[Gameboard.Board]] =
    boards.foldLeft(Map[Int,List[Gameboard.Board]]()) {
      case (acc, (k, rbs)) => if(rbs.contains(board)) acc else acc + (k -> rbs)
    }

  def boardListToBoxVector(bs:List[Gameboard.Board]):Vector[Box] = {
    def boardListToBoxVector(bs:List[Gameboard.Board], acc:Vector[Box]):Vector[Box] = bs match {
      case h :: t => boardListToBoxVector(t, Box(h) +: acc)
      case Nil    => acc
    }
    boardListToBoxVector(bs, Vector[Box]())
  }

  /** Take a list of game states and turn it into a flat deduped list */
  def gameStatesToUniqueStatesList(gs:BoxNode):List[Gameboard.Board] = {
    val gameStatesList = gs.toList
    val gameBoardsDeepList =
      gameStatesList.foldLeft(List[List[Gameboard.Board]]()){
        (acc, bn) =>
          Gameboard.createTranslatedSet(bn.board) :: acc
      }
    val m = BoxNode.deepListToMap(gameBoardsDeepList)
    BoxNode.dedupeMoves(m).foldLeft(List[Gameboard.Board]()){case (acc, (_, b)) => b :: acc}
  }

  def deepListToMap(dl:List[List[Gameboard.Board]]):Map[Int, List[Gameboard.Board]] = {
    def deepListToMap(i:Int, dl:List[List[Gameboard.Board]], acc:Map[Int, List[Gameboard.Board]]):Map[Int, List[Gameboard.Board]] = dl match {
      case h :: t => deepListToMap(i+1, t, acc + (i -> h))
      case Nil => acc
    }
    deepListToMap(0, dl, Map[Int, List[Gameboard.Board]]())
  }
}
